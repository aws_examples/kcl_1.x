package com.amazonaws.services.kinesis.samples.stocktrades.common;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;

public class YandexCredentialsProvider implements AWSCredentialsProvider {
    static class YandexCredentials implements AWSCredentials {
        @Override
        public String getAWSAccessKeyId() {
            return "";
        }

        @Override
        public String getAWSSecretKey() {
            return "";
        }
    }

    @Override
    public AWSCredentials getCredentials() {
        return new YandexCredentials();
    }

    @Override
    public void refresh() {

    }
}
