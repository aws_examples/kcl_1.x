# KCL 1.X example
### Prepare
#### credentials
Add your access key and secret key to corresponding methods of `src/main/java/com/amazonaws/services/kinesis/samples/stocktrades/common/YandexCredentialsProvider.java`

### Run your binaries
#### writer
```
$ writer <stream_name> <region_name> <kinesis_endpoint>
$ writer stream ru-central1 https://yds.serverless.yandexcloud.net/ru-central1/the/rest/of/the/endpoint
```

#### consumer
```
$ consumer <stream_name> <region_name> <kinesis_endpoint> <dynamodb_endpoint>
$ consumer stream ru-central1 https://yds.serverless.yandexcloud.net/ru-central1/the/rest/of/the/endpoint https://docapi.serverless.yandexcloud.net/ru-central1/the/rest/of/the/endpoint
```

### Known issues
#### Rersharding
KCL doesn't recognise on the fly shard count changes so far. So the only way is to do it in a few steps:
- stop the consumers;
- change the number of shards;
- remove existing LeaseTable in YDB;
- restart the consumers.
