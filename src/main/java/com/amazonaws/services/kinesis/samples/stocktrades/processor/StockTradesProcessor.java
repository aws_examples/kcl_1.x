/*
 * Copyright 2012-2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.amazonaws.services.kinesis.samples.stocktrades.processor;

import java.net.InetAddress;
import java.util.UUID;

import com.amazonaws.SDKGlobalConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.BillingMode;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.amazonaws.services.kinesis.metrics.interfaces.MetricsLevel;
import com.amazonaws.services.kinesis.samples.stocktrades.common.*;

/**
 * Sample Amazon Kinesis Application.
 */
public final class StockTradesProcessor {
    // Initial position in the stream when the application starts up for the first time.
    // Position can be one of LATEST (most recent data) or TRIM_HORIZON (oldest available data)
    private static final InitialPositionInStream SAMPLE_APPLICATION_INITIAL_POSITION_IN_STREAM =
            InitialPositionInStream.LATEST;

    private static AWSCredentialsProvider credentialsProvider;

    private static void init() {
        java.security.Security.setProperty("networkaddress.cache.ttl", "60");
        System.setProperty(SDKGlobalConfiguration.AWS_CBOR_DISABLE_SYSTEM_PROPERTY, "true");
    }

    public static void main(String[] args) throws Exception {
        String streamName = args[0];
        String regionName = args[1];
        String kinesisEndpoint = args[2];
        String dynamoDbEndpoint = args[3];
        String applicationName = "kcl1x_" + streamName;

        init();
        credentialsProvider = new YandexCredentialsProvider();
        String workerId = InetAddress.getLocalHost().getCanonicalHostName() + ":" + UUID.randomUUID();
        KinesisClientLibConfiguration configuration =
                new KinesisClientLibConfiguration(applicationName,
                        streamName,
                        credentialsProvider,
                        workerId);
        configuration.withInitialPositionInStream(SAMPLE_APPLICATION_INITIAL_POSITION_IN_STREAM);
        configuration.withSkipShardSyncAtStartupIfLeasesExist(false);
        configuration.withMetricsLevel(MetricsLevel.NONE);
        configuration.withRegionName(regionName);
        configuration.withKinesisEndpoint(kinesisEndpoint);
        configuration.withDynamoDBEndpoint(dynamoDbEndpoint);
        configuration.withBillingMode(BillingMode.PAY_PER_REQUEST);
        configuration.withCleanupLeasesUponShardCompletion(true);

        IRecordProcessorFactory recordProcessorFactory = new StockTradesRecordProcessorFactory();
        AmazonKinesisClient kinesisClient = new AmazonKinesisClient(configuration.getKinesisCredentialsProvider(),
                configuration.getKinesisClientConfiguration());
        kinesisClient.setSignerRegionOverride(regionName);

        AmazonDynamoDBClient dynamoDbClient = new AmazonDynamoDBClient(configuration.getDynamoDBCredentialsProvider(),
                configuration.getDynamoDBClientConfiguration());

        Worker worker = new Worker(recordProcessorFactory,
                configuration,
                kinesisClient,
                dynamoDbClient,
                new AmazonCloudWatchClient()
        );

        System.out.printf("Running %s to process stream %s as worker %s...\n",
                applicationName,
                streamName,
                workerId);

        int exitCode = 0;
        try {
            worker.run();
        } catch (Throwable t) {
            System.err.println("Caught throwable while processing data.");
            t.printStackTrace();
            exitCode = 1;
        }
        System.exit(exitCode);
    }
}
