package com.amazonaws.services.kinesis.samples.stocktrades.writer;

import com.amazonaws.SDKGlobalConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.kinesis.AmazonKinesisAsync;
import com.amazonaws.services.kinesis.AmazonKinesisAsyncClientBuilder;
import com.amazonaws.services.kinesis.model.*;
import com.amazonaws.services.kinesis.samples.stocktrades.common.*;

import java.nio.ByteBuffer;

public class StockTradesWriter {
    private static final org.apache.logging.log4j.Logger LOG = org.apache.logging.log4j.LogManager.getLogger(StockTradesWriter.class);

    private static void validateStream(AmazonKinesisAsync kinesisClient, String streamName) {
        try {
            DescribeStreamRequest describeStreamRequest = new DescribeStreamRequest().withStreamName(streamName);
            DescribeStreamResult describeStreamResult = kinesisClient.describeStream(describeStreamRequest);
            if(!describeStreamResult.getStreamDescription().getStreamStatus().toString().equals("ACTIVE")) {
                System.err.println("Stream " + streamName + " is not active. Please wait a few moments and try again.");
                System.exit(1);
            }
        } catch (Exception e) {
            System.err.println("Error found while describing the stream " + streamName);
            System.err.println(e);
            System.exit(1);
        }
    }

    private static void listShards(AmazonKinesisAsync kinesisClient, String streamName) {
        try {
            ShardFilter shardFilter = new ShardFilter();
            shardFilter.setType("AT_TRIM_HORIZON");
            ListShardsRequest listShardsRequest = new ListShardsRequest().withStreamName(streamName).withShardFilter(shardFilter);
            ListShardsResult listShardsResult = kinesisClient.listShards(listShardsRequest);
            System.err.println("Stream '" + streamName + "' has " + listShardsResult.getShards().size() + " shards");
            for (Shard shard:
                 listShardsResult.getShards()) {
                LOG.info("Shard: " + shard.toString());
            }
        } catch (Exception e) {
            System.err.println("Error found while describing the stream " + streamName);
            System.err.println(e);
            System.exit(1);
        }
    }
    /**
     * Uses the Kinesis client to send the stock trade to the given stream.
     *
     * @param trade instance representing the stock trade
     * @param kinesisClient Amazon Kinesis client
     * @param streamName Name of stream
     */
    private static void sendStockTrade(StockTrade trade,
                                       AmazonKinesisAsync kinesisClient,
                                       String streamName) {
        byte[] bytes = trade.toJsonAsBytes();
        // The bytes could be null if there is an issue with the JSON serialization by the Jackson JSON library.
        if (bytes == null) {
            LOG.warn("Could not get JSON bytes for stock trade");
            return;
        }

        LOG.info("Putting trade: " + trade.toString());
        PutRecordRequest request = new PutRecordRequest()
                .withPartitionKey(trade.getTickerSymbol()) // We use the ticker symbol as the partition key, explained in the Supplemental Information section below.
                .withStreamName(streamName)
                .withData(ByteBuffer.wrap(bytes));
        PutRecordResult putRecordResult = kinesisClient.putRecord(request);
    }


    public static void main(String[] args) throws Exception {
        String streamName = args[0];
        String regionName = args[1];
        String endpoint = args[2];

        System.setProperty(SDKGlobalConfiguration.AWS_CBOR_DISABLE_SYSTEM_PROPERTY, "true");
        System.setProperty(SDKGlobalConfiguration.AWS_REGION_SYSTEM_PROPERTY, regionName);

        AWSCredentialsProvider yandexCredentialsProvider = new YandexCredentialsProvider();
        AmazonKinesisAsync kinesisClient = AmazonKinesisAsyncClientBuilder.standard()
            .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, regionName))
            .withCredentials(yandexCredentialsProvider)
            .build()
        ;

        // Validate that the stream exists and is active
        validateStream(kinesisClient, streamName);
        listShards(kinesisClient, streamName);

        // Repeatedly send stock trades with a 100 milliseconds wait in between
        StockTradeGenerator stockTradeGenerator =
                new StockTradeGenerator();
        while(true) {
            com.amazonaws.services.kinesis.samples.stocktrades.common.StockTrade trade = stockTradeGenerator.getRandomTrade();
            sendStockTrade(trade, kinesisClient, streamName);
            Thread.sleep(100);
        }
    }

}

